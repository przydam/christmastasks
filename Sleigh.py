import Gift
import Elf

class Sleigh:

    def __init__(self, sleigh_id, gifts, elfs):
        self.sleigh_id = sleigh_id
        self.gifts = gifts
        self.elfs = elfs

    def __str__(self):
        return "Sanie o ID: " + str(self.sleigh_id) + " zawieraja:\nPrezenty: " + str(self.gifts) + "\nElfy: " + str(self.elfs)
