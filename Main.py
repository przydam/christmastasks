import random
import uuid
from typing import List

import gift as gift

from Elf import Elf
from Gift import Gift

sleigh = None
# list_elfs = []
# list_gifts = []
from Sleigh import Sleigh

christmas_coin = 150


def add_elf():
    name_elf = input("Podaj imię elfa którego chcesz utworzyć")
    rand_number = random.randint(0, 4)
    plik = open("files/elfs.txt", "a")
    id_elf_temp = str(uuid.uuid4())
    if rand_number == 3:
        level = -4
        elf = Elf(id_elf_temp, name_elf, level)
        print("Utworzyłem zlośliwego Elfa o imieniu " + str(name_elf))
        plik.write(id_elf_temp + ";" + name_elf + ";" + str(level) + "\n")
    else:
        level = 1
        elf = Elf(id_elf_temp, name_elf, level)
        print("Utworzyłem pozytywnego Elfa o imieniu " + str(name_elf))
        plik.write(id_elf_temp + ";" + name_elf + ";" + str(level) + "\n")

    # list_elfs.append(elf)

    plik.close()

    menu()


def show_elf():
    #  for element in list_elfs:
    #      print(element)

    plik = open("files/elfs.txt", "r")
    for line in plik:
        list_elfs_min = line.strip().split(";")
        print("Elf " + list_elfs_min[1] + " o levelu " + list_elfs_min[2] + " id " + list_elfs_min[0])
    plik.close()
    menu()


def add_elf_level(id_elf):
    i = 0
    rand_number = random.randint(0, 10)
    temporary_list_elfs = []

    plik = open("files/elfs.txt", "r")
    for line in plik:
        list_temp = line.strip().split(";")
        elf = Elf(list_temp[0], list_temp[1], int(list_temp[2]))
        temporary_list_elfs.append(elf)
    plik.close()

    for element in temporary_list_elfs:

        if str(id_elf) == str(element.elf_id):
            if rand_number == 7:
                temporary_list_elfs[i].elf_level = temporary_list_elfs[i].elf_level + 3
                print("PREZENT Świąteczny\nUlepszylem elfa o id " + str(id_elf) + " o 3 levele")
            else:
                temporary_list_elfs[i].elf_level = temporary_list_elfs[i].elf_level + 1
                print("ulepszylem elfa o id " + str(id_elf))
            break
        i += 1

    plik = open("files/elfs.txt", "w").close()
    plik = open("files/elfs.txt", "w")

    for element in temporary_list_elfs:
        plik.write(str(element.elf_id) + ";" + str(element.elf_name) + ";" + str(element.elf_level) + "\n")

    plik.close()


def upgrade_elf():
    global christmas_coin
    id_elf = input("podaj id elfa ktorego chcesz ulepszyć")
    plik = open("files/elfs.txt", "r")
    is_in_file = True
    for line in plik:
        list = line.strip().split(";")

        # for element in list_elfs:
        if id_elf == str(list[0]):
            is_in_file = False
            if int(list[2]) < 5:
                if christmas_coin < 10:
                    print("Przykro mi masz za mało pieniędzy aby ulepszyć elfa o id " + str(id_elf))
                else:
                    add_elf_level(id_elf)
                    print("Elf " + list[1] + " został ulepszony")
                    christmas_coin -= 10
            else:
                if christmas_coin < 15:
                    print("Przykro mi masz za mało pieniędzy aby ulepszyć elfa o id " + str(id_elf))
                else:
                    add_elf_level(id_elf)
                    print("Elf " + list[1] + " został ulepszony")
                    christmas_coin -= 15

    plik.close()
    if is_in_file:
        print("Nie ma elfa o takim id")
    menu()


def show_gift():
    #  for element in list_gifts:
    #    print(element)
    plik = open("files/gifts.txt", "r")
    for line in plik:
        list_gifs_temp = line.strip().split(";")
        print("Prezent " + list_gifs_temp[1] + " o levelu " + list_gifs_temp[2] + " id " + list_gifs_temp[0])

    plik.close()

    menu()


def sum_level_elfes():
    sum_levels = 0
    plik = open("files/elfs.txt", "r")
    for line in plik:
        list = line.strip().split(";")
        sum_levels += int(list[2])

    plik.close()

    # for element in list_elfs:
    #     sum_levels += element.elf_level
    return sum_levels


def sum_level_gifts():
    sum_levels_gifts = 0
    plik = open("files/gifts.txt", "r")
    for line in plik:
        list = line.strip().split(";")
        sum_levels_gifts += int(list[2])

    plik.close()

    # for element in list_elfs:
    #     sum_levels += element.elf_level
    return sum_levels_gifts


def make_gift():
    global christmas_coin
    name_gift = input("Podaj nazwę prezentu")
    level_gift = int(input("Podaj poziom trudności"))
    rand_number = random.randint(0, 30)
    if rand_number == 7:
        rand_number *= 3
        print("Świąteczna promocja pomnożyliśmy sumę leveli elfów razy 3")

    if int(sum_level_elfes()) < level_gift:
        print("Nie można utworzyć prezentu\n"
              "Elfy nie mają wystarczających umiejętności\n"
              "mają sumę umiejętności " + str(sum_level_elfes()) + ", a ty chciałeś prezent o levelu "
              + str(level_gift))
    else:
        print("Utworzyłem prezent")
        id_elf_temp = str(uuid.uuid4())
        gift = Gift(id_elf_temp, name_gift, level_gift)
        plik = open("files/gifts.txt", "a")
        plik.write(id_elf_temp + ";" + str(name_gift) + ";" + str(level_gift) + "\n")
        plik.close()

        # list_gifts.append(gift)

        christmas_coin += level_gift * 3

    menu()


def get_gift_from_stock(id_gift):
    plik = open("files/gifts.txt", "r")
    for line in plik:
        list_gifs_temp = line.strip().split(";")
        if id_gift == list_gifs_temp[0]:
            gift_object = Gift(list_gifs_temp[0], list_gifs_temp[1], list_gifs_temp[2])
            plik.close()
            return gift_object
    plik.close()
    return None


def get_elf_from_stock(id_elf):
    plik = open("files/elfs.txt", "r")
    for line in plik:
        list_elfs_temp = line.strip().split(";")
        if id_elf == list_elfs_temp[0]:
            elf_object = Elf(list_elfs_temp[0], list_elfs_temp[1], list_elfs_temp[2])
            plik.close()
            return elf_object
    plik.close()
    return None


def create_sleigh():
    global sleigh
    #   temp_list_of_gifts = []
    #   temp_list_of_elfs = []

    id_gift = input("Podaj id prezentu który chcesz wysłać")
    #    temp_list_of_gifts.append()
    id_elf = input("Podaj id elfa którego chcesz wysłać")

    id_sleight_temp = str(uuid.uuid4())
    if get_gift_from_stock(id_gift) is None:
        print("Nie ma takiego Prezentu oraz nie ma takiego Elfa sanie są puste")
        menu()
    if get_elf_from_stock(id_elf) is None:
        print("Nie ma takiego Elfa")
        menu()

    sleigh = Sleigh(id_sleight_temp, get_gift_from_stock(id_gift), get_elf_from_stock(id_elf))
    print("Utworzyłem sanie")
    print(sleigh)
    menu()


def remove_elf_from_stock(elf):
    temporary_list_elfs = []

    plik = open("files/elfs.txt", "r")
    for line in plik:
        list_temp = line.strip().split(";")
        elf_temp = Elf(list_temp[0], list_temp[1], int(list_temp[2]))
        temporary_list_elfs.append(elf)
    plik.close()
    i = 0
    for element in temporary_list_elfs:
        if str(elf.elf_id) == str(element.elf_id):
            del temporary_list_elfs[i]
        break
        i += 1


    plik = open("files/elfs.txt", "w").close()
    plik = open("files/elfs.txt", "w")

    for element in temporary_list_elfs:
        plik.write(str(element.elf_id) + ";" + str(element.elf_name) + ";" + str(element.elf_level) + "\n")

    plik.close()


def remove_gift_from_stock(gift):
    temporary_list_gifts = []

    plik = open("files/gifts.txt", "r")
    for line in plik:
        list_temp = line.strip().split(";")
        gift_temp = Gift(list_temp[0], list_temp[1], int(list_temp[2]))
        temporary_list_gifts.append(gift)
    plik.close()
    i = 0
    for element in temporary_list_gifts:
        if str(gift.gift_id) == str(element.gift_id):
            del temporary_list_gifts[i]
        break
        i += 1

    plik = open("files/gifts.txt", "w").close()
    plik = open("files/gifts.txt", "w")

    for element in temporary_list_gifts:
        plik.write(str(element.gift_id) + ";" + str(element.gift_name) + ";" + str(element.gift_difficulty) + "\n")

    plik.close()


def send_sleigh():
    global sleigh
   # print("sanie są\n"+sleigh)

    if sleigh is None:
        print("nie masz załadowanych sań, Zapakuj je")
        create_sleigh()
    else:
        print("Masz zapakowane sanie!!!!! spróbujmy je wysłać na misję")
        rand_number = random.randint(0, 100)
        chance = (sum_level_elfes() / sum_level_gifts()) * 100
        if rand_number < chance:
            print("udało się dostarczyć prezenty")
            print("Szanse ("+str(chance)+") były większe od losu ("+str(rand_number)+")")
            remove_gift_from_stock(sleigh.gifts)
            remove_elf_from_stock(sleigh.elfs)
            sleigh = None
            print("MISJA ZAKONCZONA, WESOLYCH SWIAT Z PREZENTAMI ;)")
        else:
            print("Nie udało się dostarczyć prezentów")
            print("Szanse (" + str(chance) + ") były mniejsze od losu (" + str(rand_number) + ")")
            menu()


def menu():
    global christmas_coin
    print("""======================MENU========================\nStan konta """ + str(christmas_coin) + """
1. Dodaj Elfa
2. Wyświetl Elfy
3. Ulepsz elfa
4. Wyprodukuj prezent
5. Lista wytworzonych prezentów
6. Zapakuj prezenty do sań
7. Wyślij sanie
   """)
    choose = int(input())

    if choose == 1:
        add_elf()
    if choose == 2:
        show_elf()
    if choose == 3:
        upgrade_elf()
    if choose == 4:
        make_gift()
    if choose == 5:
        show_gift()
    if choose == 6:
        create_sleigh()
    if choose == 7:
        send_sleigh()


menu()
