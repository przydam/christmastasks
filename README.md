#### Zadanie świąteczne

1. Utwórz klasę: `Elf` z atrybutami
* `id`
* `name`
* `level`

2. Utwórz klasę: `Gift` z atrybutami
* `name`
* `difficulty`

3. Utwórz zmienną globalną `Integer christmasCoin`

4. Utwórz Menu

```
1. Dodaj Elfa
2. Wyświetl Elfy
3. Ulepsz elfa
4. Wyprodukuj prezent
5. Lista wytworzonych prezentów
```

##### Dodawanie Elfa

Podczas dodawania elfa użytkownik zostanie zapytany o imie elfa.
Domyślnie elf tworzy się z poziomem `1`, id jest generowane (uuid).

##### Wyświetlanie Eflów

Wyświetla wszystkie elfy wraz z ich wszystkimi cechami

##### Ulepszanie Elfa

Ta opcja umożliwia ulepszenie elfa o 1 poziom.
Ulepszenie kosztuje
```
1 - 4lvl   10 ChristmasCoinów
5 - 10lvl  15 ChristmasCoinów
```

##### Wyprodukuj prezent

Użytkownik zostanie poproszony o nazwe prezentu, poziom trudności wytworzenia przedmiotu.
Jeśli poziom trudności wytworzenia prezentu jest większa niż suma leveli elfów, to wtedy
prezent nie zostanie utworzony.

Jeśli jednak elfy poziadają większą sume leveli, to prezent zostanie utworzony i
przybędzie `gift dificulty * 3` ChristmasCoinów.

##### Lista wyprodukowanych prezentów

Zwróci liste wyprodukowanych prezentów

#### Dodatek do zadania
##### Bonusowe Poziomy elfa
Podczas ulepszania elfa wylosuj liczbe od 0 do 10. Jeśli ta liczba jest równa 7 to ulepsz elfa o 3 poziomy.

##### Bonusowe punkty
Podczas tworzenia prezentu wylosuj liczbe od 0 do 30, jeśli wylosowana liczba to 7, to pomnóż sume siły elfów o 3

##### Elf złośnik
Podczas tworzenia elfa wylosuj liczbe od 0 do 4, jeśli wylosowana liczba to 3, to elf utworzy się z levelem -3.


### Misja świąteczna 

Utwórz klasę Sleigh. Klasa będzie odwzorowywać sanie.
Będzie posiadać następujące atrybuty:
* `id`
* `gifts`
* `elves`

###### Misja świąteczna

Dodaj w menu nową opcje dodaj sanie

Dodaj w menu nową opcje dostarcz prezenty.

=================MENU==================== 
1. Dodaj Elfa
2. Wyświetl Elfy
3. Ulepsz elfa
4. Wyprodukuj prezent
5. Lista wytworzonych prezentów
6. Zapakuj prezenty do sań
7. Wyślij sanie

<br>

## Dodawanie sań
Po wybraniu opcji dodaj sanie, użytkownik zobaczy komunikat

``` 
Podaj id prezentu
```

* jeśli podane id istnieje, to zostanie dodany prezent do sań, a usunięty z głównej listy prezenetów.
* W przeciwnym razie, zostanie powiadomiony, że nie ma takiego prezentu.
* Po poprawnym dodaniu prezentu, użytkownik zostanie zapytany, czy dodać kolejny prezent.
* Po dodaniu prezentów, użytkownik zostanie zapytany o dodanie elfa
* Dodaj wszystkie elementy do obiektu sanie (ang. Sleigh)

<br>

## Wyślij sanie - misja

Wyświetl komunikat

```
Wybierz sanie które chcesz wysłać
```

Jeśli użytkownik wybrał sanie które istnieją, to wyświetli się komunikat
```
wysyłanie sań
```

Określ szanse na pomyślne wysłanie sań wg wzoru `(szansa = sumaPoziomówElfów / sumaTrudnościWytworzeniaPrzemiotów) * 100)`

* Wylosuj liczbe od 0 do 100

jeśli liczba wylosowana jest mniejsza od wartości zmiennej `szansa`, to użytkownik dostanie komunikat 

```
udało się dostarczyć prezenty
```

W przeciwnym razie, użytkownik dostanie komunikat

```
Nie udało się dostarczyć prezentów 
``` 


